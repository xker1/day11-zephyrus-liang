import { useState } from 'react';
import { TodoGenerator } from "./TodoGenerator";
import { TodoGroup } from './TodoGroup';
import "./TodoList.css"

export const TodoList = () => {
    const [inputValue, setInputValue] = useState("")
    const [items, setItems] = useState([])

    const handleButtonChange = () => {
        setItems((prevItems) => [...prevItems, inputValue]);
        setInputValue("");
    }

    return (
        <div className="todo-list">
            <h1>TodoList</h1>
            <TodoGroup items={items} className="todo-group"/>
            <TodoGenerator inputValue={inputValue} onButtonClick={handleButtonChange} onChange={ setInputValue } />
        </div>
    )
}

export default TodoList;