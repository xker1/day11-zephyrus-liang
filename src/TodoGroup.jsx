import TodoItem from "./TodoItem";
import"./TodoGroup.css"

export const TodoGroup = ({ items }) => {
    return (
        <div className="todo-group">
            {items.map((item, index) => (
                <TodoItem key={index} text={item} />
            ))}
        </div>
    );
};
export default TodoGroup;
