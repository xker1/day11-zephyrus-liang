import "./TodoItem.css"
export const TodoItem = (props) => {
    return (
    <div className="todo-item">
      <p className="todo-item-text">{props.text}</p>
    </div>
  );
}
export default TodoItem;