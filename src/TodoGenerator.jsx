import "./TodoGenerator.css"
export const TodoGenerator = ({ inputValue, onButtonClick, onChange}) => {
    const handleButtonClick = () => {
        onButtonClick();
    };

    const handleInputChange = (e) => {
        onChange(e.target.value);
    }
    
    return (
        <div>
            <input type="text" value={inputValue} onChange={handleInputChange} />
            <button className="button" onClick={handleButtonClick}>Add</button>
        </div>
    )
}

export default TodoGenerator;